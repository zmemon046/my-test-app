import React from 'react';

const CalculatorButton = (props) => {
    const {str,setStr,item}=props
    return (
        <button onClick={function (){
            setStr(str+item)
        }}>{item}</button>
    );
};

export default CalculatorButton;