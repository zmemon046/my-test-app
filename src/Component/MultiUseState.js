import React,{useState} from 'react';

const MultiUseState = () => {

    var [number, setnumber] = useState(0)
    var [str, setstr] = useState("hello vai")
    var [bool, setbool]= useState(true)
    return (
        <div>
            <p>Number = {number}</p>
            <button onClick={function (){
                setnumber(number+1)
            }}>++</button>


            <p>greetings ={str}</p>
            <button onClick={function (){
                setstr(str+" hello vai")
            }}>Add greetings</button>

            <p>statement ={bool.toString()}</p>
            <button onClick={function (){
                setbool(!bool)
            }}>New statement</button>
        </div>
    );
};

export default MultiUseState;