import React,{useState} from 'react';
import NewStriker from "./NewStriker";

const Striker = () => {

    const [str,setStr]=useState("Hello Striker")
    const [str2,setStr2]=useState("Hello Striker2")
    const [str3,setStr3]=useState("Hello Striker3")
    return (
        <div>
            <NewStriker  str={str} greet={str2} hello={str3}/>
        </div>
    );
};

export default Striker;