import React,{useState} from 'react';

const SingleUseState = () => {
    var [obj, setobj]= useState({
        number: 0,
        str: "Hi",
        bool: true
    })

    return (
        <div>
            <p>Number = {obj.number}</p>
            <button onClick={function (){
                setobj({
                    ...obj,
                    number: obj.number+1
                })
            }}>++</button>

            <p>greetings ={obj.str}</p>
            <button onClick={function (){
                setobj({
                    ...obj,
                    str: obj.str+" Hi"
                })
            }}>Add greetings</button>

            <p>statement ={obj.bool.toString()}</p>
            <button onClick={function (){
                setobj({
                    ...obj,
                    bool: !obj.bool
                })
            }}>New statement</button>
        </div>
    );
};

export default SingleUseState;