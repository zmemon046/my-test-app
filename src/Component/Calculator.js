import React,{useState} from 'react';

import CalculatorButton from "./CalculatorButton";

const arr=[
    "1", "2","3","4","5","6","7","8","9","*","0","#"
]

const Calculator = () => {
    const [str,setStr]= useState("")
    return (
        <div>
            <p>{str == "" ? "0" : str}</p>
            {
                arr.map(function (item,index){
                    return <CalculatorButton str={str} setStr={setStr} item={item}/>
                })
            }

        </div>
    );
};

export default Calculator;